// by c1sar

	$(document).ready(
	function() {
		
		
		$('#paisSelect').change(function()
		{
			estadoval=$("#paisSelect").val();
			estados(estadoval);
					
		});
		
		$('#oficinaSelect').change(function()
		{
			oficinaval=$("#oficinaSelect").val();

			switch (oficinaval)
			{
				case "0":   
					$(".input").val("");
					$('#map').empty();
				break; 
				case "1":   
					$('#IDdireccion').val("4ta. Avenida con 6ta Transversal, Los Palos Grandes, Municipio. Chacao, 1060, Caracas.");
					$('#IDgerente').val("César Contreras");
					$('#IDemail').val("caracas@travelscarrental.com");
					$('#IDreservaciones').val("0212-284-4432");
					$('#IDfax').val("0212-284-4491");
					$('#IDhorario').val("Lunes a Domingo: 06:00 a.m. a 08:00p.m. Reservaciones telefónicas 24 horas.");
					$(document).ready(function() 
					{
  						$('#map').gMap({ markers: 
						[
							{latitude: 10.505213, longitude: -66.843210} 
						], 
                   		zoom: 16 
               			});
					});
				break; 
				case "2":   
					$('#IDdireccion').val("Av Intercomunal del Aeropuerto Internacional, Maiquetía 1161. La Guaira.");
					$('#IDgerente').val("Fiorella Aguila");
					$('#IDemail').val("aeropuerto.maiquetia@travelscarrental.com");
					$('#IDreservaciones').val("0212-991-2203");
					$('#IDfax').val("0212-991-2204");
					$('#IDhorario').val("Lunes a Domingo: 06:00 a.m. a 08:00p.m. Reservaciones telefónicas 24 horas.");
					$(document).ready(function() 
					{
  						$('#map').gMap({ markers: 
						[
							{latitude: 10.601220, longitude: -66.991235} 
						], 
                   		zoom: 16 
               			});
					});
				break; 
				case "3":   
					$('#IDdireccion').val("Avenida Guayana, Guayana City 8050. Puerto Ordaz. Ciudad Bolívar.");
					$('#IDgerente').val("Eloise Chávez");
					$('#IDemail').val("aeropuerto.puerto.ordaz@travelscarrental.com");
					$('#IDreservaciones').val("0281-242-1436");
					$('#IDfax').val("0281-242-1435");
					$('#IDhorario').val("Lunes a Domingo: 06:00 a.m. a 08:00p.m. Reservaciones telefónicas 24 horas.");   
					$(document).ready(function() 
					{
  						$('#map').gMap({ markers: 
						[
							{latitude: 10.495117, longitude: -66.811568} 
						], 
                   		zoom: 16 
               			});
					});
				break; 
				case "4":   
					$('#IDdireccion').val("Av. Monseñor Adams, entre Av. Andrés Eloy Blanco y Av. Bolívar Norte, Valencia, 2001.");
					$('#IDgerente').val("Eric Contreras");
					$('#IDemail').val("valencia.1@travelscarrental.com ");
					$('#IDreservaciones').val("0241- 824-5000");
					$('#IDfax').val("0241-824-5003");
					$('#IDhorario').val("Lunes a Domingo: 06:00 a.m. a 08:00p.m. Reservaciones telefónicas 24 horas.");  
					$(document).ready(function() 
					{
  						$('#map').gMap({ markers: 
						[
							{latitude: 10.214294, longitude: -68.010349} 
						], 
                   		zoom: 16 
               			});
					}); 
				break; 
				case "5":   
					$('#IDdireccion').val("Av Don Manuel Belloso, San Francisco. Maracaibo, 4002.");
					$('#IDgerente').val("Veronica Clapton");
					$('#IDemail').val("aeropuerto.maracaibo@travelscarrental.com ");
					$('#IDreservaciones').val("0261-791-1114");
					$('#IDfax').val("0261-791-1129");
					$('#IDhorario').val("Lunes a Domingo: 06:00 a.m. a 08:00p.m. Reservaciones telefónicas 24 horas.");
					$(document).ready(function() 
					{
  						$('#map').gMap({ markers: 
						[
							{latitude: 10.55806, longitude: -71.72778} 
						], 
                   		zoom: 16 
               			});
					});  
				break; 
				case "6":   
					$('#IDdireccion').val("Prolongación Av. Paseo Colón, Urb. Los Yaques, Puerto La Cruz, 6023.");
					$('#IDgerente').val("Isaac Tisdale");
					$('#IDemail').val("paseo.colon@travelscarrental.com");
					$('#IDreservaciones').val("0281-267-9427");
					$('#IDfax').val("0281-267-9318");
					$('#IDhorario').val("Lunes a Domingo: 06:00 a.m. a 08:00p.m. Reservaciones telefónicas 24 horas.");
					$(document).ready(function() 
					{
  						$('#map').gMap({ markers: 
						[
							{latitude: 10.216026, longitude: -64.643093} 
						], 
                   		zoom: 16 
               			});
					});  
				break; 
				case "7":   
					$('#IDdireccion').val("Calle La Mira con Boulevard Playa El Agua. Playa El Agua - Antolín del Campo, Isla de Margarita, 6301.");
					$('#IDgerente').val("Laura Pellas");
					$('#IDemail').val("playa.el.agua@travelscarrental.com");
					$('#IDreservaciones').val("0295-400-3371");
					$('#IDfax').val("");
					$('#IDhorario').val("Lunes a Domingo: 06:00 a.m. a 08:00p.m. Reservaciones telefónicas 24 horas.");
					$(document).ready(function() 
					{
  						$('#map').gMap({ markers: 
						[
							{latitude: 11.144235, longitude: -63.862828} 
						], 
                   		zoom: 16 
               			});
					});    
				break;
				case "8":   
					$('#IDdireccion').val("Sector El Yaque, Isla de Margarita 6301.");
					$('#IDgerente').val("Johanna Stevens");
					$('#IDemail').val("aeropuerto.margarita@travelscarrental.com");
					$('#IDreservaciones').val("0295-242-4580");
					$('#IDfax').val("");
					$('#IDhorario').val("Lunes a Domingo: 06:00 a.m. a 08:00p.m. Reservaciones telefónicas 24 horas.");
					$(document).ready(function() 
					{
  						$('#map').gMap({ markers: 
						[
							{latitude: 10.900011, longitude: - 63.968608} 
						], 
                   		zoom: 16 
               			});
					});
				break;
				default:   

			}//switch

		});
	});