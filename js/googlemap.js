google.maps.event.addDomListener(window, 'load', init);
        
function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 6,

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(8.93333333, -67.43333330000002), // New York

        // How you would like to style the map. 
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{featureType:"water",elementType:"all",stylers:[{hue:"#76aee3"},{saturation:38},{lightness:-11},{visibility:"on"}]},{featureType:"road.highway",elementType:"all",stylers:[{hue:"#8dc749"},{saturation:-47},{lightness:-17},{visibility:"on"}]},{featureType:"poi.park",elementType:"all",stylers:[{hue:"#c6e3a4"},{saturation:17},{lightness:-2},{visibility:"on"}]},{featureType:"road.arterial",elementType:"all",stylers:[{hue:"#cccccc"},{saturation:-100},{lightness:13},{visibility:"on"}]},{featureType:"administrative.land_parcel",elementType:"all",stylers:[{hue:"#5f5855"},{saturation:6},{lightness:-31},{visibility:"on"}]},{featureType:"road.local",elementType:"all",stylers:[{hue:"#ffffff"},{saturation:-100},{lightness:100},{visibility:"simplified"}]},{featureType:"water",elementType:"all",stylers:[]}]
    };
    
    // Get the HTML DOM element that will contain your map 
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');

    // Create the Google Map using out element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);
    google.maps.event.addListener(map, 'zoom_changed', function() {
        if (map.getZoom() < 6) map.setZoom(6);
    });
    var markerImage = new google.maps.MarkerImage('img/marker.png',
        new google.maps.Size(48,48),
        new google.maps.Point(0,0)
    );
    
    var oficinaPalosGrandes = new google.maps.LatLng(10.505213,-66.843210);
    var oficinaMaiquetia = new google.maps.LatLng(10.601220,-66.991235);
    var oficinaPuertoOrdaz = new google.maps.LatLng(10.495117,-66.811568);
    var oficinaValencia = new google.maps.LatLng(10.214294,-68.010349);
    var oficinaMaracaibo = new google.maps.LatLng(10.55806,-71.72778);
    var oficinaPuertoLaCruz = new google.maps.LatLng(10.216026,-64.643093);
    var oficinaPlayaElAgua = new google.maps.LatLng(11.144235,-63.862828);
    var oficinaSantiagoMarino = new google.maps.LatLng(10.900011,-63.968608);
    
    var marker = new google.maps.Marker({
        icon:markerImage,
        position: oficinaPalosGrandes,
        map: map,
        title: 'Oficina Palos Grandes',
        animation: google.maps.Animation.DROP
    });
    var markerMaiquetia = new google.maps.Marker({
        icon:markerImage,
        position: oficinaMaiquetia,
        map: map,
        title: 'Oficina Maiquetía',
        animation: google.maps.Animation.DROP
    });
    var markerPuertoOrdaz = new google.maps.Marker({
        icon:markerImage,
        position: oficinaPuertoOrdaz,
        map: map,
        title: 'Oficina Puerto Ordaz',
        animation: google.maps.Animation.DROP
    });
    var markerValencia = new google.maps.Marker({
        icon:markerImage,
        position: oficinaValencia,
        map: map,
        title: 'Oficina Valencia',
        animation: google.maps.Animation.DROP
    });
    var markerMaracaibo = new google.maps.Marker({
        icon:markerImage,
        position: oficinaMaracaibo,
        map: map,
        title: 'Oficina Maracaibo',
        animation: google.maps.Animation.DROP
    });
    var markerPuertoLaCruz = new google.maps.Marker({
        icon:markerImage,
        position: oficinaPuertoLaCruz,
        map: map,
        title: 'Oficina Puerto La Cruz',
        animation: google.maps.Animation.DROP
    });
    var markerPlayaElAgua = new google.maps.Marker({
        icon:markerImage,
        position: oficinaPlayaElAgua,
        map: map,
        title: 'Oficina Playa El Agua',
        animation: google.maps.Animation.DROP
    });
    var markerSantiagoMarino = new google.maps.Marker({
        icon:markerImage,
        position: oficinaSantiagoMarino,
        map: map,
        title: 'Oficina Santiago Mariño',
        animation: google.maps.Animation.DROP
    });
    
    google.maps.event.addListener(marker, 'click', function() {
        map.setZoom(15);
        map.setCenter(marker.getPosition());
    });
    google.maps.event.addListener(markerMaiquetia, 'click', function() {
        map.setZoom(15);
        map.setCenter(markerMaiquetia.  getPosition());
    });
    google.maps.event.addListener(markerPuertoOrdaz, 'click', function() {
        map.setZoom(15);
        map.setCenter(markerPuertoOrdaz.getPosition());
    });
    google.maps.event.addListener(markerValencia, 'click', function() {
        map.setZoom(15);
        map.setCenter(markerValencia.getPosition());
    });
    google.maps.event.addListener(markerMaracaibo, 'click', function() {
        map.setZoom(15);
        map.setCenter(markerMaracaibo.getPosition());
    });
    google.maps.event.addListener(markerPuertoLaCruz, 'click', function() {
        map.setZoom(15);
        map.setCenter(markerPuertoLaCruz.getPosition());
    });
    google.maps.event.addListener(markerPlayaElAgua, 'click', function() {
        map.setZoom(15);
        map.setCenter(markerPlayaElAgua.getPosition());
    });
    google.maps.event.addListener(markerSantiagoMarino, 'click', function() {
        map.setZoom(15);
        map.setCenter(markerSantiagoMarino.getPosition());
    });
}
