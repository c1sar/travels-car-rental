/* CapacidadPersonas, Puertas, Maletas, AireAcondicionado, Transmisión, directorio */
var carroCelta = ["4","2","1","Si","Sincrónico", "img/carrosEconomicos/carroCelta/"];
var carroRenaultClio = ["5","2","1","Si","Sincrónico", "img/carrosEconomicos/carroRenaultClio/"];
var carroFiatPalio = ["5","4","1","Si","Sincrónico", "img/carrosEconomicos/carroFiatPalio/"];
var carroEtios = ["5","4","2","Si","Sincrónico", "img/carrosCompactos/carroEtios/"];
var carroPrisma = ["5","4","2","Si","Sincrónico", "img/carrosCompactos/carroPrisma/"];
var carroCorsa = ["5","4","2","Si","Sincrónico", "img/carrosCompactos/carroCorsa/"];
var carroToyota = ["5","4","2","Si","Automático","img/carrosEstandar/carroToyota/"];
var carroOptra = ["5","2","2","Si","Sincrónico", "img/carrosEstandar/carroOptra/"];
var carroZafari = ["7","4","4","Si","Sincrónico", "img/carrosGrandes/carroZafira/"];
$(document).on('ready',function(){
    
    $('#carroEconomico').on('click',function(){
        $('#menuCompacto').hide();
        $('#menuGrande').hide();
        $('#menuEstandar').hide();
        $('#menuEconomico').show();
        $('#carroCompacto').removeClass("seleccionado");
        $('#carroGrande').removeClass("seleccionado");
        $('#carroEstandar').removeClass("seleccionado");        
        $('#carroEconomico').addClass("seleccionado");
    });
    $('#carroCompacto').on('click',function(){
        $('#menuCompacto').show();
        $('#menuEconomico').hide()
        $('#menuGrande').hide()
        $('#menuEstandar').hide();
        $('#carroEconomico').removeClass("seleccionado");
        $('#carroEstandar').removeClass("seleccionado");
        $('#carroGrande').removeClass("seleccionado");
        $('#carroCompacto').addClass("seleccionado");
        
    });
    $('#carroEstandar').on('click',function(){
        $('#carroEconomico').removeClass("seleccionado");
        $('#carroCompacto').removeClass("seleccionado");
        $('#carroGrande').removeClass("seleccionado");
        $('#carroEstandar').addClass("seleccionado");
        $('#menuEstandar').show();
        $('#menuEconomico').hide();
        $('#menuGrande').hide();
        $('#menuCompacto').hide();
        
    });
    $('#carroGrande').on('click',function(){
        $('#carroEconomico').removeClass("seleccionado");
        $('#carroCompacto').removeClass("seleccionado");
        $('#carroEstandar').removeClass("seleccionado");
        $('#carroGrande').addClass("seleccionado");
        $('#menuEstandar').hide();
        $('#menuEconomico').hide();
        $('#menuGrande').show();
        $('#menuCompacto').hide();
        
    });
    $('#menuCompacto').hide();
    $('#menuEstandar').hide();
    $('#menuGrande').hide();
    $('#fotoC1').on('click',function(){
        var fotoSeleccionada = $('#fotoC1').attr("src");
        var fotoPrincipal = $('#fotoPrincipal').attr("src");
        $('#fotoC1').attr({
            'src': fotoPrincipal
        });
        $('#fotoPrincipal').attr({
            'src': fotoSeleccionada
        });
    });
    $('#fotoC2').on('click',function(){
        var fotoSeleccionada = $('#fotoC2').attr("src");
        var fotoPrincipal = $('#fotoPrincipal').attr("src");
        $('#fotoC2').attr({
            'src': fotoPrincipal
        });
        $('#fotoPrincipal').attr({
            'src': fotoSeleccionada
        });
    });
    $('#carroCelta').on('click',function(){
       fotoPrincipal = carroCelta[5] + "foto0.jpg";
        foto1 = carroCelta[5] + "foto1.jpg";
        foto2 = carroCelta[5] + "foto2.jpg";
        $('#fotoPrincipal').attr({
            'src': fotoPrincipal
        });
        $('#fotoC1').attr({
            'src': foto1
        });
        $('#fotoC2').attr({
            'src': foto2
        });
        $('#tituloCarro').text('GM Celta');
        $('#capacidadPersonas').text(carroCelta[0]);
        $('#cantidadPuertas').text(carroCelta[1]);
        $('#transmision').text(carroCelta[4]);
        $('#aireActivo').text(carroCelta[3]);
        $('#capacidadMaletas').text(carroCelta[2]);
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
    $('#carroRenaultClio').on('click',function(){
        fotoPrincipal = carroRenaultClio[5] + "foto0.jpg";
        foto1 = carroRenaultClio[5] + "foto1.jpg";
        foto2 = carroRenaultClio[5] + "foto2.jpg";
        $('#fotoPrincipal').attr({
            'src': fotoPrincipal
        });
        $('#fotoC1').attr({
            'src': foto1
        });
        $('#fotoC2').attr({
            'src': foto2
        });
        $('#tituloCarro').text('Renault Clio');
        $('#capacidadPersonas').text(carroRenaultClio[0]);
        $('#cantidadPuertas').text(carroRenaultClio[1]);
        $('#transmision').text(carroRenaultClio[4]);
        $('#aireActivo').text(carroRenaultClio[3]);
        $('#capacidadMaletas').text(carroRenaultClio[2]);
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        
    });
    $('#carroFiatPalio').on('click',function(){
        fotoPrincipal = carroFiatPalio[5] + "foto0.jpg";
        foto1 = carroFiatPalio[5] + "foto1.jpg";
        foto2 = carroFiatPalio[5] + "foto2.jpg";
        $('#fotoPrincipal').attr({
            'src': fotoPrincipal
        });
        $('#fotoC1').attr({
            'src': foto1
        });
        $('#fotoC2').attr({
            'src': foto2
        });
        $('#tituloCarro').text('Fiat Palio');
        $('#capacidadPersonas').text(carroFiatPalio[0]);
        $('#cantidadPuertas').text(carroFiatPalio[1]);
        $('#transmision').text(carroFiatPalio[4]);
        $('#aireActivo').text(carroFiatPalio[3]);
        $('#capacidadMaletas').text(carroFiatPalio[2]);
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
    
    $('#carroCorsa').on('click',function(){
       fotoPrincipal = carroCorsa[5] + "foto0.jpg";
        foto1 = carroCorsa[5] + "foto1.jpg";
        foto2 = carroCorsa[5] + "foto2.jpg";
        $('#fotoPrincipal').attr({
            'src': fotoPrincipal
        });
        $('#fotoC1').attr({
            'src': foto1
        });
        $('#fotoC2').attr({
            'src': foto2
        });
        $('#tituloCarro').text('Corsa Classic');
        $('#capacidadPersonas').text(carroCorsa[0]);
        $('#cantidadPuertas').text(carroCorsa[1]);
        $('#transmision').text(carroCorsa[4]);
        $('#aireActivo').text(carroCorsa[3]);
        $('#capacidadMaletas').text(carroCorsa[2]);
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
    $('#carroEtios').on('click',function(){
        fotoPrincipal = carroEtios[5] + "foto0.jpg";
        foto1 = carroEtios[5] + "foto1.jpg";
        foto2 = carroEtios[5] + "foto2.jpg";
        $('#fotoPrincipal').attr({
            'src': fotoPrincipal
        });
        $('#fotoC1').attr({
            'src': foto1
        });
        $('#fotoC2').attr({
            'src': foto2
        });
        $('#tituloCarro').text('Toyota Etios XS');
        $('#capacidadPersonas').text(carroEtios[0]);
        $('#cantidadPuertas').text(carroEtios[1]);
        $('#transmision').text(carroEtios[4]);
        $('#aireActivo').text(carroEtios[3]);
        $('#capacidadMaletas').text(carroEtios[2]);
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        
    });
    $('#carroPrisma').on('click',function(){
        fotoPrincipal = carroPrisma[5] + "foto0.jpg";
        foto1 = carroPrisma[5] + "foto1.jpg";
        foto2 = carroPrisma[5] + "foto2.jpg";
        $('#fotoPrincipal').attr({
            'src': fotoPrincipal
        });
        $('#fotoC1').attr({
            'src': foto1
        });
        $('#fotoC2').attr({
            'src': foto2
        });
        $('#tituloCarro').text('GM Prisma');
        $('#capacidadPersonas').text(carroPrisma[0]);
        $('#cantidadPuertas').text(carroPrisma[1]);
        $('#transmision').text(carroPrisma[4]);
        $('#aireActivo').text(carroPrisma[3]);
        $('#capacidadMaletas').text(carroPrisma[2]);
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
    $('#carroOptra').on('click',function(){
        fotoPrincipal = carroOptra[5] + "foto0.jpg";
        foto1 = carroOptra[5] + "foto1.jpg";
        foto2 = carroOptra[5] + "foto2.jpg";
        $('#fotoPrincipal').attr({
            'src': fotoPrincipal
        });
        $('#fotoC1').attr({
            'src': foto1
        });
        $('#fotoC2').attr({
            'src': foto2
        });
        $('#tituloCarro').text('Chevrolet Optra');
        $('#capacidadPersonas').text(carroOptra[0]);
        $('#cantidadPuertas').text(carroOptra[1]);
        $('#transmision').text(carroOptra[4]);
        $('#aireActivo').text(carroOptra[3]);
        $('#capacidadMaletas').text(carroOptra[2]);
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
    $('#carroToyota').on('click',function(){
        fotoPrincipal = carroToyota[5] + "foto0.jpg";
        foto1 = carroToyota[5] + "foto1.jpg";
        foto2 = carroToyota[5] + "foto2.jpg";
        $('#fotoPrincipal').attr({
            'src': fotoPrincipal
        });
        $('#fotoC1').attr({
            'src': foto1
        });
        $('#fotoC2').attr({
            'src': foto2
        });
        $('#tituloCarro').text('Toyota Corolla');
        $('#capacidadPersonas').text(carroToyota[0]);
        $('#cantidadPuertas').text(carroToyota[1]);
        $('#transmision').text(carroToyota[4]);
        $('#aireActivo').text(carroToyota[3]);
        $('#capacidadMaletas').text(carroToyota[2]);
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
    $('#carroZafira').on('click',function(){
        fotoPrincipal = carroZafari[5] + "foto0.jpg";
        foto1 = carroZafari[5] + "foto1.jpg";
        foto2 = carroZafari[5] + "foto2.jpg";
        $('#fotoPrincipal').attr({
            'src': fotoPrincipal
        });
        $('#fotoC1').attr({
            'src': foto1
        });
        $('#fotoC2').attr({
            'src': foto2
        });
        $('#tituloCarro').text('Chevrolet Zafira');
        $('#capacidadPersonas').text(carroZafari[0]);
        $('#cantidadPuertas').text(carroZafari[1]);
        $('#transmision').text(carroZafari[4]);
        $('#aireActivo').text(carroZafari[3]);
        $('#capacidadMaletas').text(carroZafari[2]);
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
});