<!-- by c1sar -->
<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8" name="description" content="Ub&iacute;canos"/>
	<title>Ubicanos</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/Venezuela.css">

    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/jquery.gmap.js"></script>
    <script type="text/javascript" src="js/mapa.js"></script>
    <script type="text/javascript" src="js/cargaOficinas.js"></script>
    <script type="text/javascript" src="js/validacion.js"></script>

<script>
	
	var estadoActual="0";
	var estadoAnterior="0";
	var colorZulia="#99BD77";
	
</script>


</head>

<body>
	<div class="gallery-overlay"></div>
    <div class="Gallery is-tweetless">

    	<div class="Gallery-closeTarget"></div>
    	<div class="Gallery-content no-grid" style="width: 261px; min-height: 0px;"></div>

	</div>

	<nav class="menuPrin">
        <section id="containerMenu">
            <a href="#" class="logo" >
                <img src="img/LogoTravelBordeBlanco.png" alt="" width="350">
            </a>    
            <ul>
                <li><a class="icono home" href="index.html">INICIO</a></li>
                <li><a class="seleccion icono ubicanos" href="ubicanos.php">UBÍCANOS</a></li>
                <li><a class="icono flota" href="flota.html">FLOTA</a></li>
                <li><a class="icono politica" href="politica.html">POLÍTICA</a></li>
                <li><a class="icono contacto" href="contactenos.html">CONTÁCTENOS</a></li>
            </ul>
        </section>
    </nav>

<br>
<br>
<br>
<br>
<br>
<br>
<br>

<section id="contenedor">
	<form  class="FORMubicanos" action="#" id="Fmapa" runat="server"> 
		<section id="SECmapa">
        	<br>
        	<table align="center" class="tabla" border="0" cellpadding="6">           
        			 <tr> 
                        <td valign=top align="right"><label class="texto" for="name">Estado: </label></td>
                        <td ><select id="paisSelect" name=pais size=1>
                            <option value="0">--Seleccione-- </option>
                            <option value="1"> Amazonas</option>
                            <option value="2"> Anzoátegui</option>
                            <option value="3"> Apure</option>
                            <option value="4"> Aragua</option>
                            <option value="5"> Barinas</option>
                            <option value="6"> Bolívar</option>
                            <option value="7"> Carabobo</option>
                            <option value="8"> Cojedes</option>
                        	<option value="9"> Delta Amacuro</option>
                        	<option value="10"> Distrito Capital</option>
                        	<option value="11"> Falcón</option>
                        	<option value="12"> Guárico</option>
                        	<option value="13"> Lara</option>
                        	<option value="14"> Mérida</option>
                        	<option value="15"> Miranda</option>
                        	<option value="16"> Monagas</option>
                        	<option value="17"> Nueva Esparta</option>
                        	<option value="18"> Portuguesa</option>
                        	<option value="19"> Sucre</option>
                        	<option value="20"> Táchira</option>
                        	<option value="21"> Trujillo</option>
                        	<option value="22"> Vargas</option>
                        	<option value="23"> Yaracuy</option>
                        	<option value="24"> Zulia</option>
    					</select></td>
                	</tr>
                    <tr>  
                    	<td align="right"><label class="texto" for="name">Oficina: </label></td>
                   		<td><select id="oficinaSelect" name=pais size=1>
                        	<option value="0">--Seleccione--</option>
    					</select></td>
                	</tr> 
                    <tr>  
                        <td valign="top" align="right"><label for="name">Dirección:</label> </td>
                        <td><textarea  id="IDdireccion" type="text" rows="3" readonly class="input"></textarea></td>
                    </tr> 
            </table>
            <br>
            <br>
			<?php include("includes/mapa.php");?>
            <br>
            <br>
            <br>
    	</section>
        
                <section id="SECform">
                	<br>
                    <table align="center" border="0" class="tabla" cellpadding="6">              
                    <tr>  
                        <td align="right"><label for="name">Gerente:</label></td> 
                        <td><input id="IDgerente" type="text" readonly class="input"></td>
                    </tr> 
                    
                    <tr>  
                        <td align="right"><label for="name">Email:</label></td> 
                        <td><input id="IDemail" type="text" readonly class="input"></td>
                    </tr> 
                    
                    <tr>  
                        <td align="right"><label for="name">Reservaciones:</label></td> 
                        <td><input id="IDreservaciones" type="text" readonly class="input"></td>
                    </tr> 
                    
                    <tr>  
                        <td align="right"><label for="name">Fax:</label></td> 
                        <td><input id="IDfax" type="text" readonly class="input"></td>
                    </tr> 
                    
                    <tr valign=top>  
                        <td valign="top" align="right"><label for="name">Horario de Atención:</label> </td>
                        <td><textarea  id="IDhorario" type="text" rows="2" readonly class="input"></textarea></td>
                    </tr>
                   
                    </table>
                    <div id="map"></div>
                    <br>
                    <br>
                </section>
        	</ul>
        </div>
    </form> 
    
    <br>
    <br>
    
    <p id="conseguir">¿No consigue una oficina cercana? <a href="#openModal" class="botonENVIAR"> Contáctenos</a> </p>
    
</section>
	
    <br>
    <br>
	<footer>
        <section class="piePagina">
            <div class="contenedorContacto">
                <h5>Contáctenos</h5>
                <h2>Teléfono: +58 212821 12 12</h2>
                <h3>Email: travel@carrental.com</h3>
                <h4>Skype: travelcar</h4>
            </div>
            <div class="contenedorRedesSociales">
                <pre>   Siganos:</pre>
                <img src="img/tw.png" alt="" width="80">
                <img src="img/fb.png" alt="" width="80">
            </div>
            <div class="copyrightFooter">
                <p>Copyright © 2014 Henrique López We Are Ucab Solutions Company No.07367478f. VAT No. 997 0297 65. All Rights Reserved </p>
            </div>
        </section>
    </footer>
    

    <div id="openModal" class="modalDialog">
        <div>
            <a href="#close" title="Close" class="close">X</a>
            <br>
            <h1 class="titulopopa">Solicitud de Apertura de Sucursal</h1>
            <form  class="FORMubicanos" action="#" id="Fmapa" runat="server">
            	<table align="center" class="tabla2" border="0" cellpadding="6">           
        			 <tr> 
                        <td width="125px" valign="top" align="right"><label class="texto" for="name">Estado: </label></td>
                        <td ><select id="paisSelect" name=pais size=1>
                            <option value="0">--Seleccione-- </option>
                            <option value="1"> Amazonas</option>
                            <option value="2"> Anzoátegui</option>
                            <option value="3"> Apure</option>
                            <option value="4"> Aragua</option>
                            <option value="5"> Barinas</option>
                            <option value="6"> Bolívar</option>
                            <option value="7"> Carabobo</option>
                            <option value="8"> Cojedes</option>
                        	<option value="9"> Delta Amacuro</option>
                        	<option value="10"> Distrito Capital</option>
                        	<option value="11"> Falcón</option>
                        	<option value="12"> Guárico</option>
                        	<option value="13"> Lara</option>
                        	<option value="14"> Mérida</option>
                        	<option value="15"> Miranda</option>
                        	<option value="16"> Monagas</option>
                        	<option value="17"> Nueva Esparta</option>
                        	<option value="18"> Portuguesa</option>
                        	<option value="19"> Sucre</option>
                        	<option value="20"> Táchira</option>
                        	<option value="21"> Trujillo</option>
                        	<option value="22"> Vargas</option>
                        	<option value="23"> Yaracuy</option>
                        	<option value="24"> Zulia</option>
    					</select></td>
                	</tr>
                    <tr>  
                        <td width="125px" valign="top" align="right"><label for="name">Dirección:</label> </td>
                        <td><textarea type="text" rows="3" class="input"></textarea></td>
                    </tr>
                    <tr>  
                        <td width="125px" valign="top" align="right"><label for="name">Nombre:</label> </td>
                        <td><input type="text" onkeypress="txNombres()" class="input"></td>
                    </tr> 
                    <tr>  
                        <td width="125px" valign="top" align="right"><label for="name">Apellido:</label> </td>
                        <td><input type="text" onkeypress="txNombres()" class="input"></td>
                    </tr> 
                    <tr>  
                        <td width="125px" valign="top" align="right"><label for="name">Correo:</label> </td>
                        <td><input type="email" class="input"></td>
                    </tr> 
                    <tr>  
                        <td width="125px" valign="top" align="right"><label for="name">Teléfono:</label> </td>
                        <td><input type="tel" onkeypress="ValidaSoloNumeros()" class="input"></td>
                    </tr>
                    <tr>  
                        <td width="125px" valign="top" align="right"><label for="name">Propuesta:</label> </td>
                        <td><textarea type="text" rows="7" class="input"></textarea></td>
                    </tr> 
                    <tr>
                    	<td> <a href="" class="botonENVIAR">ENVIAR</a> </td> 
                    </tr>
           		</table>    
        	</form>
            <br>
        </div>
    </div>
	


</body>

 




</html>
